﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VT_WPF_DLL;
using System.Data.OleDb;

namespace VT_WPF_1
{
    class Program
    {
        static void Main(string[] args)
        {
            //ins2Result();
            //DbCheck();
            //ShowResult();
            //GetPercentage();
            SingletonTest();
        }
        static void DbCheck()
        {
            Vocabulary v = new Vocabulary(3);

            foreach (Word word in v.WordList)
            {
                Console.WriteLine(word);
            }
        }
        static void ins2Result()
        {
            string cs = "Provider = Microsoft.Jet.OLEDB.4.0; Data Source = VocTrainer.mdb";// Wo Datenbank liegt und welches hilfsprogramm angewendet wird
            OleDbConnection con = new OleDbConnection(cs);
            con.Open();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = con;
            cmd.CommandText = "Insert into results(Positive,Negative,Exdate) values(2,1,'"+DateTime.Now+"')";
            cmd.ExecuteNonQuery();//für insert update und delete returntyp int von betroffenen Werte
            cmd.CommandText = "select * from results";
            
           OleDbDataReader reader= cmd.ExecuteReader();
            while (reader.Read())
            {
                Console.WriteLine(reader.GetInt32(0));
            }
            reader.Close();
            con.Close();
        }
        static void ShowResult()
        {
            string cs = "Provider = Microsoft.Jet.OLEDB.4.0; Data Source = VocTrainer.mdb";// Wo Datenbank liegt und welches hilfsprogramm angewendet wird
            OleDbConnection con = new OleDbConnection(cs);
            con.Open();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = con;
            cmd.CommandText = "select sum(Positive),sum(Negative) from results where exdate between #1/1/2018# and #1/1/2019#";
            int c = (int)cmd.ExecuteNonQuery();//einzelne Sachen
            Console.WriteLine(c);
            //ExecuteReader wenn mehrere Spalten
        }

        static void GetPercentage()
        {
            string cs = "Provider = Microsoft.Jet.OLEDB.4.0; Data Source = VocTrainer.mdb";// Wo Datenbank liegt und welches hilfsprogramm angewendet wird
            OleDbConnection con = new OleDbConnection(cs);
            con.Open();
            OleDbCommand cmd = new OleDbCommand();
            OleDbCommand update = new OleDbCommand();
            
            cmd.Connection = con;
            update.Connection = con;
           
            
            cmd.CommandText = "Select Id,Positive,Negative,Percentage from results";
            OleDbDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                if (reader.GetValue(3) == System.DBNull.Value)
                {
                    int id = reader.GetInt32(0);
                    float[] arr = new float[2];
                    arr[0] = reader.GetInt32(1);
                    arr[1] = reader.GetInt32(2);
                    float percentage = (arr[0] / (arr[0] + arr[1]))*100;
                    
                    update.CommandText="update results set percentage = '"+percentage.ToString()+"' where id= "+id;
                    update.ExecuteNonQuery();

                    
                }
            }
            reader.Close();
            con.Close();
            

        }
        static void SingletonTest()
        {
            Vocabulary v1 = new Vocabulary(3);
            Vocabulary v2 = new Vocabulary(3);

            if (v1.con.Equals(v2.con))
            {
                Console.WriteLine("EEEEEEEEEEE");
            }
            v1.CloseDateBaseCon();
        }
    }
}
