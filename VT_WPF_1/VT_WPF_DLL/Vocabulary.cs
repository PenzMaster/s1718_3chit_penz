﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using VT_WPF_1;
using VT_WPF_DLL;
using System.Data.OleDb;



namespace VT_WPF_1
{
    public class Vocabulary
    {
        List<Word> wordList;
        public List<Word> WordList { get { return wordList; } }
        private static Random rand = new Random();
        public OleDbConnection con;

        public Vocabulary(int LoadType)
        {
            con = VTConnection.GetVTConnection();
            if (LoadType == 1)
            {
                this.wordList = RandomizeWordList(LoadFV());
            }
            else if (LoadType == 2)
            {
                this.wordList = RandomizeWordList(LoadFV());
            }
            else if (LoadType == 3)
            {
                this.wordList = RandomizeWordList(LoadDB());
            }
        }
        public bool CheckWords(string german, string english)
        {
            List<Word> li = WordList.FindAll(w => w.baseWord == german);
            foreach (Word item in li)
            {
                if (item.translatedWord == english)
                    return true;
            }
            return false;
        }

        private List<Word> LoadFV()
        {
            List<Word> wordList = new List<Word>();

            wordList.Add(new VT_WPF_1.Word { baseWord = "Beispiel", translatedWord = "11" });
            wordList.Add(new VT_WPF_1.Word { baseWord = "HUnd", translatedWord = "12" });
            wordList.Add(new VT_WPF_1.Word { baseWord = "1", translatedWord = "13" });
            wordList.Add(new VT_WPF_1.Word { baseWord = "2", translatedWord = "14" });
            wordList.Add(new VT_WPF_1.Word { baseWord = "3", translatedWord = "15" });
            wordList.Add(new VT_WPF_1.Word { baseWord = "4", translatedWord = "16" });
            wordList.Add(new VT_WPF_1.Word { baseWord = "5", translatedWord = "17" });
            wordList.Add(new VT_WPF_1.Word { baseWord = "6", translatedWord = "18" });
            wordList.Add(new VT_WPF_1.Word { baseWord = "7", translatedWord = "19" });
            wordList.Add(new VT_WPF_1.Word { baseWord = "8", translatedWord = "20" });
            wordList.Add(new VT_WPF_1.Word { baseWord = "9", translatedWord = "21" });
            wordList.Add(new VT_WPF_1.Word { baseWord = "10", translatedWord = "23" });

            return wordList;











        }

        private List<Word> LoadTF()
        {
            wordList = new List<Word>();
            StreamReader sr = new StreamReader("word.txt");
            try
            {
                while (sr.Peek() != -1)
                {
                    string s = sr.ReadLine();
                    string[] words = s.Split(';');
                    wordList.Add(new Word { baseWord = words[0], translatedWord = words[1] });
                }
            }catch(Exception e)
            {
                throw e;
            }
            finally
            {
                sr.Close();
            }
            return wordList;
        }
        private List<Word> RandomizeWordList(List<Word> l)
        {
            int n = l.Count;
            while (n > 1)
            {
                n--;
                int k = rand.Next(n + 1);
                Word Word = l[k];
                l[k] = l[n];
                l[n] = Word;
            }
            return l;
        }

        private List<Word> LoadDB()
        {
            //string cs = "Provider = Microsoft.Jet.OLEDB.4.0; Data Source = VocTrainer.mdb";// Wo Datenbank liegt und welches hilfsprogramm angewendet wird
            //OleDbConnection con = new OleDbConnection(cs);
            con.Open();
            wordList = new List<Word>();

            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = con;
            cmd.CommandText = "select German,English from words";
            OleDbDataReader reader = cmd.ExecuteReader();

            

            while (reader.Read())
            {
                if(reader.GetValue(0)!= System.DBNull.Value && reader.GetValue(1)!=System.DBNull.Value)
                {
                wordList.Add(new Word { baseWord = reader.GetString(0), translatedWord = reader.GetString(1) });
                }
            }
            con.Close();

            return wordList;

           
        }

        public List<Word> GetWords()
        {
            throw new System.NotImplementedException();
        } 
       public void CloseDateBaseCon()
        {
            VTConnection.CloseVTConnection();
        }
    }
}