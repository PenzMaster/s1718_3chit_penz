﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;

namespace VT_WPF_DLL
{
    class VTConnection
    {
        private static OleDbConnection instance = null;

        static public OleDbConnection GetVTConnection()
        {
            if (instance == null)
            {
                instance = new OleDbConnection("Provider = Microsoft.Jet.OLEDB.4.0; Data Source = VocTrainer.mdb");
            }
            return instance;
        }
       static public void CloseVTConnection()
        {
                if(instance != null)
                {
                    instance.Close();
                }
        }
    }
}
