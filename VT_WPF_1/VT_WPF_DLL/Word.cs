﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace VT_WPF_1
{
    public class Word
    {
        public string baseWord { get; set; }

        public string translatedWord { get; set; }

        public override string ToString()
        {
            return baseWord+" "+translatedWord;
        }
    }
}