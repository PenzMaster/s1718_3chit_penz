﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VTrainer;

namespace VokabelTrainer1
{
    public partial class Form1 : Form
    {
        Vokabeltrainer VokabelT;
        //CreatePanel c = new CreatePanel();
        SetUp optionen = new SetUp();
        public Form1()
        {

            InitializeComponent();
            VokabelT = new Vokabeltrainer();
            //Klasse Vokabeltrainer besitzt GetList() und Check() Methode
            //Vokabel besitzt properties von deutschen und englishen Wörtern
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int vocs = 0;
            int correct = 0;

            foreach (Control item in panel1.Controls)
            {
                if (optionen.hilf[1] == "EN-DE")
                {
                    if (item is TextBox)// Überprüft items ob sie Textboxen sind 
                    {
                        vocs++;
                        if (VokabelT.Check(item.Text, (string)item.Tag))//Check Methode überprüft ob deutscher und englischer String gleich sind (liefert true oder false zurück
                        {
                            item.BackColor = Color.Green;
                            correct++;
                        }
                        else
                        {
                            item.BackColor = Color.Red;
                        }
                    }
                }
                else
                {
                    if (item is TextBox)// Überprüft items ob sie Textboxen sind 
                    {
                        vocs++;
                        if (VokabelT.Check((string)item.Tag, item.Text))//Check Methode überprüft ob deutscher und englischer String gleich sind (liefert true oder false zurück
                        {
                            item.BackColor = Color.Green;
                            correct++;
                        }
                        else
                        {
                            item.BackColor = Color.Red;
                        }
                    }

                }
            }

            double result = (double)correct / (double)vocs * (double)100;

            optionen.Read();
            //optionen.CreatePanelList
            for (int i = 0; i < optionen.CreatePanelList.Count; i++)
            {
                if (result >= Convert.ToDouble(optionen.CreatePanelList[i].Percent.Text))
                {
                    MessageBox.Show((optionen.CreatePanelList[i].Grade.Text));
                    break;
                }
                else
                {
                    MessageBox.Show((optionen.CreatePanelList[i].Grade.Text));
                    
                }
            }

        }

        private void newTestToolStripMenuItem_Click(object sender, EventArgs e)//wenn NEW TEST gedrückt wird
        {
            panel1.Controls.Clear();
            optionen.Read();
            if (optionen.hilf[1] == "EN-DE")
            {
                List<Vokabel> listeVokabel = VokabelT.GetList();
                int count = 0;
                if (listeVokabel.Count <= optionen.Anzahl)
                {
                    count = listeVokabel.Count;

                }
                else
                {
                    count = optionen.Anzahl;
                }


                for (int i = 0; i < optionen.Anzahl; i++)
                {
                    Label l = new Label();
                    TextBox t = new TextBox();

                    l.Top = 10 + i * 30;
                    t.Top = 10 + i * 30;
                    l.Left = 10;
                    t.Left = 120;

                    l.Text = listeVokabel[i].English;//Label wird mit der Liste von deutschen Wörter gefüllt
                    t.Tag = listeVokabel[i].English;// Tag wird auch mit der Liste von deutschen Wörter gefüllt

                    panel1.Controls.Add(l);//Label zum panel hinzufügen
                    panel1.Controls.Add(t);//Textbox zum panel hinzufügen
                }
            }
            else
            {
                List<Vokabel> listeVokabel = VokabelT.GetList();
                int count = 0;
                if (listeVokabel.Count <= optionen.Anzahl)
                {
                    count = listeVokabel.Count;

                }
                else
                {
                    count = optionen.Anzahl;
                }


                for (int i = 0; i < optionen.Anzahl; i++)
                {
                    Label l = new Label();
                    TextBox t = new TextBox();

                    l.Top = 10 + i * 30;
                    t.Top = 10 + i * 30;
                    l.Left = 10;
                    t.Left = 120;

                    l.Text = listeVokabel[i].German;//Label wird mit der Liste von deutschen Wörter gefüllt
                    t.Tag = listeVokabel[i].German;// Tag wird auch mit der Liste von deutschen Wörter gefüllt

                    panel1.Controls.Add(l);//Label zum panel hinzufügen
                    panel1.Controls.Add(t);//Textbox zum panel hinzufügen


                }





            }

        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int i;
            SetUpDialog s = new SetUpDialog();


            optionen.Read();
            s.Controls.AddRange(optionen.CreatePanelList.ToArray());

            if (optionen.hilf != null && optionen.hilf[0] != null && optionen.hilf[1] != null)
            {
                if (Int32.TryParse(optionen.hilf[0], out i))
                {
                    s.numericUpDown1.Value = i;
                }
                else
                {
                    s.numericUpDown1.Value = 0;
                }

                if (optionen.hilf[1] == "EN-DE")
                {
                    s.comboBox1.SelectedIndex = 1;
                }
                else
                    s.comboBox1.SelectedIndex = 0;


            }
            else
            {
                s.numericUpDown1.Value = 0;
                s.comboBox1.SelectedIndex = 0;
            }







            DialogResult ergebnis = s.ShowDialog();

            if (ergebnis == DialogResult.OK)
            {
                optionen.Anzahl = (int)s.numericUpDown1.Value;
                optionen.Sprache = s.comboBox1.Text;
                optionen.CreatePanelList = new List<CreatePanel>();


                foreach (var item in s.Controls)
                {
                    if (item is CreatePanel)
                    {
                        optionen.CreatePanelList.Add((CreatePanel)item);
                    }
                }


                optionen.Write();

            }
            if (ergebnis == DialogResult.Cancel)
            {
                s.Close();
            }
        }
    }
}
