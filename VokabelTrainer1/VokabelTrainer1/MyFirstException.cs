﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace VokabelTrainer1
{
    class MyFirstException : Exception
    {
        public MyFirstException()
        {

        }
        public MyFirstException(string message) : base(message)
        {

        }
        public MyFirstException(string message, Exception inner) : base(message, inner)
        {
            MessageBox.Show(message);
            WriteError(inner);
        }

        public void WriteError(Exception e)
        {
            if (File.Exists("vt_errors.log"))
            {
                StreamWriter sw = new StreamWriter("vt_errors.log", true);
                try
                {


                    sw.WriteLine(DateTime.Now + " " + Message);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    sw.Flush();
                    sw.Close();
                }

            }



        }

    }
}
