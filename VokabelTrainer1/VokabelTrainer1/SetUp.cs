﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace VokabelTrainer1
{
    class SetUp
    {
        //CreatePanel c = new CreatePanel();

        private bool sp_isDirty = false;
        private bool anz_isDirty = false;
        private int anzahl;
        private string sprache;
        public string[] hilf;
        //public int percentage;
        //public string grade;
        //public double value;

        private List<CreatePanel> createPanelList;

        public List<CreatePanel> CreatePanelList
        {
            get { return createPanelList; }
            set { createPanelList = value; }
        }



        public string Sprache
        {
            get { return this.sprache; }

            set
            {
                if (this.sprache != value)
                {
                    this.sprache = value;
                    this.sp_isDirty = true;
                }
            }
        }
        public int Anzahl
        {
            get { return this.anzahl; }

            set
            {
                if (this.anzahl != value)
                {
                    this.anzahl = value;
                    this.anz_isDirty = true;
                }
            }
        }

        public void Read()
        {
            if (File.Exists("options.txt"))
            {
                StreamReader sr = new StreamReader("options.txt");
                try
                {

                    string eingabe = sr.ReadLine();
                    hilf = eingabe.Split(';');
                    Anzahl = Int32.Parse(hilf[0]);
                    Sprache = hilf[1];
                    int y = 150;
                    int x = 10;
                    CreatePanelList = new List<CreatePanel>();
                    while (sr.Peek() != -1)
                    {

                        string buffer = sr.ReadLine();
                        string[] buffer2;
                        CreatePanel panel = new CreatePanel(x, y += 25);
                        buffer2 = buffer.Split(';');
                        panel.Percent.Text = buffer2[0];
                        panel.Grade.Text = buffer2[1];
                        panel.Value.Value = Convert.ToDecimal(buffer2[2]);
                        CreatePanelList.Add(panel);




                    }
                }
                catch (FormatException ex)
                {
                    throw new MyFirstException("geht nd", ex);

                }
               
                catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                finally
                {
                    sr.Close();
                }

            }



        }


        public void Write()
        {

            if ((sp_isDirty || anz_isDirty))
            {
                if (File.Exists("options.txt"))
                { 
                StreamWriter sw = new StreamWriter("options.txt", false);

                try
                {

                    sw.Write(anzahl + ";");
                    sw.Write(sprache + ";");
                    sw.WriteLine("//");
                    foreach (var item in this.CreatePanelList)
                    {

                        sw.WriteLine(item.Percent.Text + ";" + item.Grade.Text + ";" + item.Value.Value);



                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Fehler ist aufgetreten" + ex.Message, ex);
                }
                finally
                {
                    sw.Flush();
                    sw.Close();
                }
            }






            }

        }

        public SetUp()
        {
            Read();
        }
    }
}
