﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lift
{
    class Lift
    {
        public string LiftID { get; set; }
        public double MaxW { get; set; }

        private List<Person> liste = new List<Person>();

        private double gesamt = 0;

        public delegate void Pointer(Lift l, Person b);
        public event Pointer overload;

        public void Load(Person p)
        {
            if (gesamt + p.weight <= MaxW)
            {
                liste.Add(p);
                gesamt += p.weight;
            }
            else
                this.overload(this, p);
        }

        public override string ToString()
        {
            string ausgabe = "";
            foreach (Person item in liste)
            {
                ausgabe += item;
            }

            return ausgabe;
           
        }

    }
}
