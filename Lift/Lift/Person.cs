﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lift
{
    class Person
    {
        public double weight { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return Name + " " + weight;
        }
    }
}
